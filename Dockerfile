ARG PHP_IMG_TAG=8.0.8-apache

# container
FROM php:$PHP_IMG_TAG

RUN a2enmod rewrite
RUN service apache2 restart

